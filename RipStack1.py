from twisted.internet.protocol import Protocol, Factory
from zope.interface.declarations import implements
from twisted.internet.interfaces import ITransport, IStreamServerEndpoint

from playground.network.message.StandardMessageSpecifiers import BOOL1, \
    STRING, UINT2, UINT4, LIST, DEFAULT_VALUE, OPTIONAL
from playground.network.message.ProtoBuilder import MessageDefinition
from playground.network.common.Protocol import StackingTransport,\
    StackingFactoryMixin, StackingProtocolMixin

from playground.network.common.statemachine import StateMachine
from playground.network.common.statemachine import StateMachineError

from playground.network.common.Packet import Packet, PacketStorage, IterateMessages
from playground.playgroundlog import packetTrace, logging
from playground.error import GetErrorReporter
from playground.network.common.Protocol import MessageStorage
from playground.network.common import CertFactory
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA256
from playground.crypto import X509Certificate

import os, random



"""
    Step 1: Define Rip Message Body
"""

class RipMessage(MessageDefinition):
    PLAYGROUND_IDENTIFIER = "RipProtocol.RipStack.RipMessage"
    MESSAGE_VERSION = "1.0"
    BODY = [
	("sequence_number", UINT4, OPTIONAL),
	("acknowledgement_number", UINT4, OPTIONAL),
	("sequence_number_notification_flag", BOOL1, DEFAULT_VALUE(False)),
	("certificate", LIST(STRING), OPTIONAL),
	("signature", STRING, DEFAULT_VALUE("")),
	("SNNACK", BOOL1, DEFAULT_VALUE(False)),
	("ACK", BOOL1, DEFAULT_VALUE(False)),
        ("data", STRING, DEFAULT_VALUE(""))
     ]

"""
    Step 2: Define Rip Transport
"""

class RipTransport(StackingTransport):
    def __init__(self, lowerTransport, protocol):
        StackingTransport.__init__(self, lowerTransport)
        self.protocol = protocol

    def write(self, data):
        print "In rip transport: data send --> " + data
        self.protocol.dataSend(data)


"""
    Step 3: Define Rip Protocol (FSM link each state and signal)
"""

class RipProtocol(StackingProtocolMixin, Protocol):

    # States for handshake
    STATE_CLOSE = "RIP STATE MACHINE: CLOSE"
    STATE_LISTEN = "RIP STATE MACHINE: LISTEN"
    STATE_ESTABLISHED = "RIP STATE MACHINE: ESTABLISHED"
    STATE_SNN_RECEIVED = "RIP STATE MACHINE: SNN RECEIVED"
    STATE_SNN_SENT = "RIP STATE MACHINE: SNN SEND"
 	
    # Client signal
    SIGNAL_SEND_SNN = "RIP CLIENT SIGNAL: send snn"
    SIGNAL_RCVD_SNNACK = "RIP CLIENT SIGNAL: receive snnack"
    
    # Send signal
    SIGNAL_RCVD_SNN = "RIP SEND SIGNAL: receive snn"
    SIGNAL_RCVD_ACK = "RIP SEND SIGNAL: receive ack"

    SIGNAL_RIPMESSAGE = "RIP SIGNAL: general data"

  
    def __init__(self):
      
	self.storage = MessageStorage()
	self.SM = StateMachine("Rip Test State Machine")
	self.sequence_number = None

    def dataSend(self, data):
        if (self.SM.currentState() == self.STATE_ESTABLISHED):
            ripMsg = RipMessage()
            ripMsg.data = data
            self.transport.write(ripMsg.__serialize__())
	else:
	    return

    def dataReceived(self, data):
	#print "in protocol data received" + data
	self.storage.update(data)
	for msg in self.storage.iterateMessages():
   	    self.handleSignals(msg)
    	
    def intToNonce(self,i):
	h = hex(i)
	h = h[2:] #remove 0x
	if h[-1] == 'L': #remove "L"
	    h = h[:-1]
	return h


    def verifyAndHash(self, sig, data, certBytes):
        cert = X509Certificate.loadPEM(certBytes)
	peerPublicKeyBlob = cert.getPublicKeyBlob()
	peerPublicKey = RSA.importKey(peerPublicKeyBlob)
	rsaVerifier = PKCS1_v1_5.new(peerPublicKey)
	hasher = SHA256.new()
	hasher.update(data)
        result = rsaVerifier.verify(hasher, sig)
	return result

    def signAndHash(self, data, address):
	rawKey = CertFactory.getPrivateKeyForAddr(address)
	rsaKey = RSA.importKey(rawKey)
	rsaSigner = PKCS1_v1_5.new(rsaKey)
	hasher = SHA256.new()
	hasher.update(data)
	hashedSignatureBytes = rsaSigner.sign(hasher)
	#print hashedSignatureBytes + "signature bytes are"
	return hashedSignatureBytes

    def loadCerts(self, certificates, address):
        chainCertificates = CertFactory.getCertsForAddr(address)
	certificates = certificates + chainCertificates
        return certificates

    def verifyCerts(self, chainCertificates):
	rootCert = CertFactory.getRootCert()
	if chainCertificates[-1] != rootCert:
	    chainCertificates.append(rootCert)
	firstCert = chainCertificates[0]
	firstCert = X509Certificate.loadPEM(firstCert)
	for c in chainCertificates[1:]:
	    c = X509Certificate.loadPEM(c)
	    if firstCert.getIssuer() != c.getSubject(): 
		return False
	
	    issuePublicKeyBlob = c.getPublicKeyBlob()
	    issuePublicKey = RSA.importKey(issuePublicKeyBlob)
	    issueVerifier = PKCS1_v1_5.new(issuePublicKey)
	    bytesToVerify = firstCert.getPemEncodedCertWithoutSignatureBlob()
	    hasher = SHA256.new()
	    hasher.update(bytesToVerify)
	    if not issueVerifier.verify(hasher, firstCert.getSignatureBlob()):
		return False
	    firstCert = c
	return True
	
		  
"""
    Step 3: RipServerProtocol & RipClientProtocol
"""

class RipServerProtocol(RipProtocol):
    def __init__(self):
	self.storage = MessageStorage()
	self.SM = StateMachine("Rip Receive Protocol")
	self.SM.addState(self.STATE_LISTEN,(self.SIGNAL_RCVD_SNN, self.STATE_SNN_RECEIVED))

	self.SM.addState(self.STATE_SNN_RECEIVED, (self.SIGNAL_RCVD_ACK, self.STATE_ESTABLISHED), onEnter=self.sendSnnAck)
	self.SM.addState(self.STATE_ESTABLISHED, (self.SIGNAL_RIPMESSAGE, self.STATE_ESTABLISHED), onEnter=self.handleMessage)
	
	self.nonce = None
	self.sequence_number = random.randint(200,400)

    def connectionMade(self):
        self.SM.start(self.STATE_LISTEN)
	print "serveer State1 " + self.SM.currentState()
	

    def handleSignals(self, msg):
	if msg.sequence_number_notification_flag == True:
	    Signal = self.SIGNAL_RCVD_SNN
	    self.SM.signal(Signal, msg)
	elif msg.ACK == True:
	    Signal = self.SIGNAL_RCVD_ACK
	    self.SM.signal(Signal, msg)
	else:
	    Signal = self.SIGNAL_RIPMESSAGE
	    self.SM.signal(Signal, msg)
	
    # need to verify signature
    def sendSnnAck(self, signal, data):
	ripMsg = data
	if signal == self.SIGNAL_RCVD_SNN:  
	    chainCertificates = ripMsg.certificate
	    verifyCertResult = self.verifyCerts(chainCertificates[1:])

	    if verifyCertResult: 
	    	sig = ripMsg.signature
	    	ripMsg.signature = ""
                ripMsgBytes = ripMsg.__serialize__()
 	   	result = self.verifyAndHash(sig, ripMsgBytes, chainCertificates[1])
	    	if result:
		    print "verification result", result
		    self.nonce = os.urandom(8).encode('hex')
	    	    snnAckMsg = RipMessage()
		    certificates = []
		    peerNonce = int(chainCertificates[0], 16) + 1
		    certificates.append(self.intToNonce(peerNonce))
		    certificates.append(self.nonce)
		    #snnAckMsg.certificate = self.loadCerts(certificates, self.transport.getHost()[0])
		    snnAckMsg.certificate = self.loadCerts(certificates, self.transport.getHost().host)
		    serverAckNum = ripMsg.sequence_number + 1
		    snnAckMsg.sequence_number = self.sequence_number
		    snnAckMsg.acknowledgement_number = serverAckNum
		    print "sequence number server", snnAckMsg.sequence_number
		    print "ack number server", snnAckMsg.acknowledgement_number
		    snnAckMsg.signature = ""
		    snnAckMsg.sequence_number_notification_flag = True
	            signatureBytes = snnAckMsg.__serialize__()
		    #hashedSignatureBytes = self.signAndHash(signatureBytes, self.transport.getHost()[0])
		    hashedSignatureBytes = self.signAndHash(signatureBytes, self.transport.getHost().host)
		    snnAckMsg.signature = hashedSignatureBytes
	    	    self.transport.write(snnAckMsg.__serialize__())
		else: 
		    print "error: hash failed"
	    else: 
		print "error: certificates failed"
	else: 
	    print "error:unknown signals "
	

    def handleMessage(self,signal, data):
	if signal == self.SIGNAL_RCVD_ACK:
	    ripMsg = data
	    chainCertificates = ripMsg.certificate
	    print "why there is an error", chainCertificates
	    sig = ripMsg.signature
	    ripMsg.signature = ""
            ripMsgBytes = ripMsg.__serialize__()
 	    result = self.verifyAndHash(sig, ripMsgBytes, chainCertificates[1])
	    if result: 
	    	if int(self.nonce, 16) + 1 == int(chainCertificates[0], 16):

	            higherTransport = RipTransport(self.transport, self)
                    self.makeHigherConnection(higherTransport)
		else:
		    print "error: failed nonce"
	    else:
		print "failed signature"
	  
   	else:
	    msg = data
	    self.higherProtocol() and self.higherProtocol().dataReceived(msg.data)
      



#This is client begins!!!!!

class RipClientProtocol(RipProtocol):
    def __init__(self):
	self.storage = MessageStorage()
	self.SM = StateMachine("Rip Send Protocol")
	self.SM.addState(self.STATE_CLOSE, (self.SIGNAL_SEND_SNN, self.STATE_SNN_SENT))
 	self.SM.addState(self.STATE_SNN_SENT, (self.SIGNAL_RCVD_SNNACK, self.STATE_ESTABLISHED), onEnter=self.sendSnn)
	self.SM.addState(self.STATE_ESTABLISHED, (self.SIGNAL_RIPMESSAGE, self.STATE_ESTABLISHED), onEnter=self.handleMessage)
	self.nonce = os.urandom(8).encode('hex')
	self.sequence_number = random.randint(0, 200)

    def connectionMade(self):
        self.SM.start(self.STATE_CLOSE)
	print "Client State1 " + self.SM.currentState()
	self.SM.signal(self.SIGNAL_SEND_SNN, "")
	print "Client State2 " + self.SM.currentState()
        
		

    def sendSnn(self, signal, data):
	if signal == self.SIGNAL_SEND_SNN: 
	    snnMessage = RipMessage()
	    snnMessage.sequence_number_notification_flag = True
	    
	    snnMessage.sequence_number = self.sequence_number
	    print "client sequence number", snnMessage.sequence_number
	    certificates = []
	    certificates.append(self.nonce)
	    #snnMessage.certificate = self.loadCerts(certificates, self.transport.getHost()[0])
	    snnMessage.certificate = self.loadCerts(certificates, self.transport.getHost().host)
	    snnMessage.signature = ""
	    signatureBytes = snnMessage.__serialize__()
	    #hashedSignatureBytes = self.signAndHash(signatureBytes, self.transport.getHost()[0])
	    hashedSignatureBytes = self.signAndHash(signatureBytes, self.transport.getHost().host)
	    snnMessage.signature = hashedSignatureBytes
	    self.transport.write(snnMessage.__serialize__())
	   
	
    def handleSignals(self, msg):
	if msg.sequence_number_notification_flag == True:
	    Signal = self.SIGNAL_RCVD_SNNACK
	    self.SM.signal(Signal, msg)
	else:
	    Signal = self.SIGNAL_RIPMESSAGE
	    self.SM.signal(Signal, msg)

	
    def handleMessage(self, signal, data):
	if signal == self.SIGNAL_RCVD_SNNACK:
	    ripMsg = data
	   
	    chainCertificates = ripMsg.certificate
	    if int(self.nonce, 16) + 1 == int(chainCertificates[0], 16):

	        verifyCertResult = self.verifyCerts(chainCertificates[2:])
	        if verifyCertResult: 
	    	  
	   	    sig = ripMsg.signature
	    	    ripMsg.signature = ""
   	    	    ripMsgBytes = ripMsg.__serialize__()
	    	    result = self.verifyAndHash(sig, ripMsgBytes, chainCertificates[2])
		    clientAckNum = ripMsg.acknowledgement_number
		    seqNum = ripMsg.sequence_number
	    	    if result:
	    	        self.sendAck(data, chainCertificates[1], clientAckNum, seqNum)
	    	        higherTransport = RipTransport(self.transport, self)
            	        self.makeHigherConnection(higherTransport)
		    else:
		        print "error: hash failed"
	        else:
		    print "error: certificate failed"
	    else:
		print "error: nonce failed"
	else:
	    msg = data
	    #print "data length youmeiyougaocuo", len(msg.data)
	    self.higherProtocol() and self.higherProtocol().dataReceived(msg.data)
	

    def sendAck(self, data, nonce, clientAckNum, seqNum):
	peerNonce = int(nonce, 16) + 1
	certificates = []
	certificates.append(self.intToNonce(peerNonce))
	AckMsg = RipMessage()
	self.sequence_number = clientAckNum
        AckMsg.sequence_number = self.sequence_number
	AckMsg.acknowledgement_number = seqNum + 1
	AckMsg.signature = ""
	print "send ack sequence number", AckMsg.sequence_number
	print "send ack ack number", AckMsg.acknowledgement_number
	AckMsg.ACK = True
	certificates = self.loadCerts(certificates, self.transport.getHost().host)
	AckMsg.certificate = certificates
	signatureBytes = AckMsg.__serialize__()
        hashedSignatureBytes = self.signAndHash(signatureBytes, self.transport.getHost().host)
	AckMsg.signature = hashedSignatureBytes
	self.transport.write(AckMsg.__serialize__())



"""
    Step 4: RipServerFactory & RipClientFactory
"""
class RipServerFactory(StackingFactoryMixin, Factory):
    protocol = RipServerProtocol

class RipClientFactory(StackingFactoryMixin, Factory):
    protocol = RipClientProtocol

ConnectFactory = RipClientFactory
ListenFactory = RipServerFactory
